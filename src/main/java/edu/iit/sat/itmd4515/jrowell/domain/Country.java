/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.jrowell.domain;

/**
 *
 * @author Jacquie
 */
public class Country {

    private long Code;
    private String Name;
    private String Continent;
    private String Region;
    private int IndepYear;
    private int Population;
    private float LifeExpectancy;
    private float GNP;
    private float GNPOld;
    private String LocalName;
    private String GovernmentForm;
    private String HeadOfState;
    private int Capital;
    private float SurfaceAres;

    public Country() {
    }

    public Country(long Code, String Name, String Continent, String Region, int IndepYear, int Population, float LifeExpectancy, float GNP, float GNPOld, String LocalName, String GovernmentForm, String HeadOfState, int Capital, float SurfaceAres) {
        this.Code = Code;
        this.Name = Name;
        this.Continent = Continent;
        this.Region = Region;
        this.IndepYear = IndepYear;
        this.Population = Population;
        this.LifeExpectancy = LifeExpectancy;
        this.GNP = GNP;
        this.GNPOld = GNPOld;
        this.LocalName = LocalName;
        this.GovernmentForm = GovernmentForm;
        this.HeadOfState = HeadOfState;
        this.Capital = Capital;
        this.SurfaceAres = SurfaceAres;
    }

    /**
     * Get the value of Capital
     *
     * @return the value of Capital
     */
    public int getCapital() {
        return Capital;
    }

    /**
     * Set the value of Capital
     *
     * @param Capital new value of Capital
     */
    public void setCapital(int Capital) {
        this.Capital = Capital;
    }

    /**
     * Get the value of HeadOfState
     *
     * @return the value of HeadOfState
     */
    public String getHeadOfState() {
        return HeadOfState;
    }

    /**
     * Set the value of HeadOfState
     *
     * @param HeadOfState new value of HeadOfState
     */
    public void setHeadOfState(String HeadOfState) {
        this.HeadOfState = HeadOfState;
    }

    /**
     * Get the value of GovernmentForm
     *
     * @return the value of GovernmentForm
     */
    public String getGovernmentForm() {
        return GovernmentForm;
    }

    /**
     * Set the value of GovernmentForm
     *
     * @param GovernmentForm new value of GovernmentForm
     */
    public void setGovernmentForm(String GovernmentForm) {
        this.GovernmentForm = GovernmentForm;
    }

    /**
     * Get the value of LocalName
     *
     * @return the value of LocalName
     */
    public String getLocalName() {
        return LocalName;
    }

    /**
     * Set the value of LocalName
     *
     * @param LocalName new value of LocalName
     */
    public void setLocalName(String LocalName) {
        this.LocalName = LocalName;
    }

    /**
     * Get the value of GNPOld
     *
     * @return the value of GNPOld
     */
    public float getGNPOld() {
        return GNPOld;
    }

    /**
     * Set the value of GNPOld
     *
     * @param GNPOld new value of GNPOld
     */
    public void setGNPOld(float GNPOld) {
        this.GNPOld = GNPOld;
    }

    /**
     * Get the value of GNP
     *
     * @return the value of GNP
     */
    public float getGNP() {
        return GNP;
    }

    /**
     * Set the value of GNP
     *
     * @param GNP new value of GNP
     */
    public void setGNP(float GNP) {
        this.GNP = GNP;
    }

    /**
     * Get the value of LifeExpectancy
     *
     * @return the value of LifeExpectancy
     */
    public float getLifeExpectancy() {
        return LifeExpectancy;
    }

    /**
     * Set the value of LifeExpectancy
     *
     * @param LifeExpectancy new value of LifeExpectancy
     */
    public void setLifeExpectancy(float LifeExpectancy) {
        this.LifeExpectancy = LifeExpectancy;
    }

    /**
     * Get the value of Population
     *
     * @return the value of Population
     */
    public int getPopulation() {
        return Population;
    }

    /**
     * Set the value of Population
     *
     * @param Population new value of Population
     */
    public void setPopulation(int Population) {
        this.Population = Population;
    }

    /**
     * Get the value of IndepYear
     *
     * @return the value of IndepYear
     */
    public int getIndepYear() {
        return IndepYear;
    }

    /**
     * Set the value of IndepYear
     *
     * @param IndepYear new value of IndepYear
     */
    public void setIndepYear(int IndepYear) {
        this.IndepYear = IndepYear;
    }

    /**
     * Get the value of SurfaceAres
     *
     * @return the value of SurfaceAres
     */
    public float getSurfaceAres() {
        return SurfaceAres;
    }

    /**
     * Set the value of SurfaceAres
     *
     * @param SurfaceAres new value of SurfaceAres
     */
    public void setSurfaceAres(float SurfaceAres) {
        this.SurfaceAres = SurfaceAres;
    }

    /**
     * Get the value of Continent
     *
     * @return the value of Continent
     */
    public String getContinent() {
        return Continent;
    }

    /**
     * Set the value of Continent
     *
     * @param Continent new value of Continent
     */
    public void setContinent(String Continent) {
        this.Continent = Continent;
    }

    /**
     * Get the value of Region
     *
     * @return the value of Region
     */
    public String getRegion() {
        return Region;
    }

    /**
     * Set the value of Region
     *
     * @param Region new value of Region
     */
    public void setRegion(String Region) {
        this.Region = Region;
    }

    /**
     * Get the value of Name
     *
     * @return the value of Name
     */
    public String getName() {
        return Name;
    }

    /**
     * Set the value of Name
     *
     * @param Name new value of Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * Get the value of Code
     *
     * @return the value of Code
     */
    public long getCode() {
        return Code;
    }

    /**
     * Set the value of Code
     *
     * @param Code new value of Code
     */
    public void setCode(long Code) {
        this.Code = Code;
    }

    @Override
    public String toString() {
        return "Country{" + "Code=" + Code + ", Name=" + Name + ", Continent=" + Continent + ", Region=" + Region + ", IndepYear=" + IndepYear + ", Population=" + Population + ", LifeExpectancy=" + LifeExpectancy + ", GNP=" + GNP + ", GNPOld=" + GNPOld + ", LocalName=" + LocalName + ", GovernmentForm=" + GovernmentForm + ", HeadOfState=" + HeadOfState + ", Capital=" + Capital + ", SurfaceAres=" + SurfaceAres + '}';
    }

}
