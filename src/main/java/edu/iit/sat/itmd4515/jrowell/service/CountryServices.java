/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.jrowell.service;

import edu.iit.sat.itmd4515.jrowell.domain.Country;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author Jacquie
 */
@Stateless
public class CountryServices {
    
    private static final Logger LOG = Logger.getLogger(CountryServices.class.getName());
    
    @Resource(lookup ="jdbc/itmd4515") 
    DataSource ds;
    

    public CountryServices() {
    }
    
    /*
     * Returns either the found customer (by CustomerId) or null
     */
    
    public Country findCountry(Long Code){
        Country country = null;
        String sql = "select * from Country where Code = ?";
      
        
        try(Connection c = ds.getConnection()){
            
           PreparedStatement ps = c.prepareStatement(sql);
           ps.setLong(1, Code);

           ResultSet rs = ps.executeQuery();
            

            if (rs.next()) {
                country = new Country(
                    rs.getLong("Code"),
                    rs.getString("Name"),
                    rs.getString("Continent"),
                    rs.getString("Region"),
                    rs.getInt("IndepYear"),
                    rs.getInt("Population"),
                    rs.getFloat("LifeExpectancy"),
                    rs.getFloat("GNP"),
                    rs.getFloat("GNPOld"),
                    rs.getString("LocalName"),
                    rs.getString("GoernmentForm"),
                    rs.getString("HeadOfState"),
                    rs.getInt("Capital"),
                    rs.getLong("Code2")
                );
             
                LOG.info(country.toString());
            }
            
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return country;
    }
}
